
diameter=14; // [8:20]
baseHeight=10; //  [1:15]
clawHeight=40; //  [5:70]
$fn=100;
difference() {
  cylinder(d=diameter+0.5, h=baseHeight);
}
translate([0,0,baseHeight*0.69]) {
rotate([0,20,0])
  difference() {
    cylinder(d1=diameter+1, d2=0, h=clawHeight);
  }
}
