
$fn=100;

poly = [
  [0 , 0],
  [30, 0],
  [30, 1],
  [10, 3],
  [10, 4],
  [0 , 4]
];
difference() {
    rotate([0,0,-30])
    rotate_extrude(angle=-120) {
        translate([100, 0])
            polygon(poly);
    }

    //color("red")
    rotate([0,0,-60])
    rotate_extrude(angle=-60) {
        translate([121, -1])
            square([10,10]);
    }
}

rotate([0,0,-30])
    ringAttachement();
rotate([0,0,-150])
    ringAttachement();

module ringAttachement() {
    translate([100, 0, 0])
        difference() {
            cylinder(r=8, h=4);
            cylinder(r=5, h=4);
        }
}

