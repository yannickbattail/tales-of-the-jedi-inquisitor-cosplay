whole cape
- [X] buy fabric
- [X] prototype
- [ ] cape
- [ ] hood
- [ ] col
- [X] flock the logo of the empire

gants
- [X] buy gloves
- [X] paint

claws
- [X] design
- [X] printing
- [ ] paint

masque
- [X] printing
- [X] paint
- [X] rubber band

sabre
- [X] printing
- [X] blades
- [X] 8-32 machine screw nut and a 1/4 inch 8-32 set screw

necklace
- [X] design
- [X] printing
- [ ] paint

clothes
- [X] pull
- [X] pantalon

ceinture
- [ ] boucle
- [ ] objets
