# tales-of-the-jedi-inquisitor-cosplay

Star Wars cosplay of the inquisitor in the serie Tales of the Jedi.


## 3D models

- [mask](https://cults3d.com/en/3d-model/various/mask-from-the-inquisitor-from-totj)
- [lightsaber](https://www.thingiverse.com/thing:1945665) form the grand inquisitor, [with de remix for blade](https://www.thingiverse.com/thing:5027377)
- My own model for the claws ([thumb](models/claws_thumb.3mf), [middle finger](models/claws_middle.3mf), [pinky finger](models/claws_pinky.3mf)) with the [sources](models/griffes.scad) (made with openSCAD)
- My own model for the [necklace](models/necklace.3mf) with the [sources](models/necklace.scad) (made with openSCAD)

## Clothes

- cape
- necklace
- hood
- gloves

Empire [logo](models/Empire_logo)
